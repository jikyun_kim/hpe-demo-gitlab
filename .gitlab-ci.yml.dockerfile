stages:
  - build
  - deploy

variables:
  APP_NAME: gitops-argocd-demo
  CI_REGISTRY_IMAGE: pcrete/$APP_NAME
  CD_CHART_REPO: webapp-chart
  CD_GIT_REPOSITORY: git@gitlab.com:gitops-argocd-demo/$CD_CHART_REPO.git
  CD_MANIFEST_FILE: Chart.yaml
  TAG: $CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA

before_script:
  - echo $CI_REGISTRY_IMAGE:$TAG $PWD
  # login
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD

#after_script:
#  - docker image prune -af

build_image:
  stage: build
  script:
    # Docker Build && Push image
    - cat Dockerfile
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - >
      docker build
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$TAG
      .
    - docker push $CI_REGISTRY_IMAGE:$TAG

tag_latest_image:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - docker pull $CI_REGISTRY_IMAGE:$TAG
    - docker tag $CI_REGISTRY_IMAGE:$TAG $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest