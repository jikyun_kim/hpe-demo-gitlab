package main
 
import (
    "net/http"
)
 
func main() {
    http.HandleFunc("/bluegreen", func(w http.ResponseWriter, req *http.Request) {
        w.Write([]byte("Hello BlueGreen - Blue"))
    })

    http.HandleFunc("/canary", func(w http.ResponseWriter, req *http.Request) {
        w.Write([]byte("Hello Canary - before canary"))
    })
 
    http.ListenAndServe(":9080", nil)
}