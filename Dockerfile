#FROM ubuntu
#ENTRYPOINT ["/bin/bash", "-c", "echo hello"]


# syntax=docker/dockerfile:1

#FROM golang:1.16-alpine

#WORKDIR /app

#COPY go.mod ./
#COPY go.sum ./
#RUN go mod download

#COPY *.go ./

#RUN go build -o main main.go
#RUN go build -o /docker-gs-ping

#EXPOSE 8080

#CMD [ "/docker-gs-ping" ]

FROM golang:alpine AS builder

ADD main.go .

RUN go build main.go

FROM alpine

COPY --from=builder /go/main /go/main

EXPOSE 9080

CMD ["/go/main"]